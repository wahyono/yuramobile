import 'package:flutter/material.dart';
import 'package:project_akhir/component/app_bar_custom.dart';
import 'package:project_akhir/component/navigation/bottom_navigation_bar_custom.dart';
import 'package:project_akhir/component/orders/orders_list.dart';
import 'package:project_akhir/data/orders/orders_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Orders extends StatefulWidget {
  _Orders createState() => _Orders();
}

class _Orders extends State<Orders> {
  int _selectedIndex = 1;

  Future<List<dynamic>> fetchOrders() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var body = await OrdersData().stats(prefs.getString("userId")!);

    if(body != false) {
      return body;
    }

    return [];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.blue,
        flexibleSpace: SafeArea(
          child: AppBarCustom(),
        ),
      ),
      body: Container(
        child: FutureBuilder<List<dynamic>> (
          future: fetchOrders(),
          builder: (context, snapshot) {
            if(snapshot.hasData) {
              return ListView.builder(
                padding: EdgeInsets.all(10),
                itemCount: snapshot.data?.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: EdgeInsets.symmetric(vertical: 5),
                    child: OrdersList(data: snapshot.data![index])
                  );
                },
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
      bottomNavigationBar: BottomNavigationBarCustom(selectedIndex: _selectedIndex),
    );
  }

}