import 'package:flutter/material.dart';
import 'package:project_akhir/component/login/login_button.dart';
import 'package:project_akhir/component/utilfs.dart';
import 'package:project_akhir/data/login/login_data.dart';
import 'package:project_akhir/service/shared_preferences_service.dart';
import 'package:toast/toast.dart';

/**
 * halaman login
 */

class Login extends StatefulWidget {
  _Login createState() => _Login();
}

class _Login extends State<Login> {
  /**
   * pada halaman login terdapat dua buah text field, yaitu email dan password
   */
  TextEditingController emailController = TextEditingController()..text = "johndoe@gmail.com";
  TextEditingController passwordController = TextEditingController()..text = "johndoe123";

  /**
   * _formKey digunakan sebagai key untuk form login
   */
  final _formKey = GlobalKey<FormState>();
  String _token = "";
  String userId = "";

  /**
   * busyView adalah tampilan progress indicator
   */
  bool busyView = false;

  /**
   * variabel ini digunakan sebagai acuan visibilitas password
   */
  bool _passwordVisible = false;

  /**
   * fungsi ini digunakan untuk mengirim data credential user ke API
   */
  Future authLogin(String email, String password) async {
    var body = await LoginData().login(email, password);

    return body;
  }

  @override
  Widget build(BuildContext context) {
    ToastContext().init(context);

    return Scaffold(
      body: Center(
        child: Container(
          child: SafeArea(
            child: Padding(
              padding: EdgeInsets.all(15),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.all(25),
                    child: Column(
                      children: [
                        Container(
                          child: Image.asset("assets/images/icon.png", scale: 5),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 25, bottom: 20),
                          child: Text(
                            "YuRa MobileApp",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.w700
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: emailController,
                          decoration: InputDecoration(
                              labelText: "Email",
                              floatingLabelStyle: TextStyle(color: Color(0xFFFD8833)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xFFC4C4C4), width: 0),
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xFFFD8833), width: 2),
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red, width: 2),
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              errorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red, width: 2),
                                  borderRadius: BorderRadius.circular(8)
                              )
                          ),
                          keyboardType: TextInputType.emailAddress,
                          validator: (value) {
                            String pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                            RegExp regex = RegExp(pattern);

                            if(!regex.hasMatch(value!)) {
                              return "Enter Valid Email";
                            } else {
                              return null;
                            }
                          },
                        ),
                        SizedBox(height: 10),
                        TextFormField(
                          controller: passwordController,
                          decoration: InputDecoration(
                              labelText: "Password",
                              suffixIcon: IconButton(
                                icon: Icon(
                                  _passwordVisible
                                      ? Icons.visibility_off : Icons.visibility,
                                  color: Color(0xFF3F4254),
                                ),
                                onPressed: () {
                                  setState(() {
                                    if(_passwordVisible) {
                                      _passwordVisible = false;
                                    } else {
                                      _passwordVisible = true;
                                    }
                                  });
                                },
                              ),
                              floatingLabelStyle: TextStyle(color: Color(0xFFFD8833)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xFFC4C4C4), width: 0),
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Color(0xFFFD8833), width: 2),
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red, width: 2),
                                  borderRadius: BorderRadius.circular(8)
                              ),
                              errorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red, width: 2),
                                  borderRadius: BorderRadius.circular(8)
                              )
                          ),
                          validator: (value) {
                            if(value!.length < 4) {
                              return "password must be at least 4 characters long";
                            } else {
                              return null;
                            }
                          },
                          obscureText: !_passwordVisible,
                        ),
                        SizedBox(height: 20),
                        LoginButton(
                          width: MediaQuery.of(context).size.width,
                          height: 50,
                          onTap: () async {
                            if(_formKey.currentState!.validate()) {
                              setState(() {
                                busyView = true;
                              });

                              /**
                               * variabel auth digunakan untuk menyimpan response API auth/login
                               */
                              var auth = await authLogin(emailController.text, passwordController.text);

                              /**
                               * jika response auth tidak false maka akan diteruskan ke halaman dashboard,
                               * jika response nya false, maka akan ditampilkan pesan dalam bentuk Toast
                               * dan variabe busyView akan di set menjadi false sehingga progress indicator
                               * akan tampil
                               */
                              if(auth != false) {
                                if(auth["status"]) {
                                  _token = auth["data"]["token"];
                                  userId = auth["data"]["id"];

                                  print(_token);
                                  UtilFs.showToast("Login successful", context);
                                  await sharedPreferenceService.setToken(_token);
                                  await sharedPreferenceService.setKey("userId", userId);
                                  Navigator.pushReplacementNamed(context, "/dashboard");
                                } else {
                                  setState(() {
                                    busyView = false;
                                  });
                                  UtilFs.showToast("Login failed", context);
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                }
                              } else {
                                setState(() {
                                  busyView = false;
                                });
                                UtilFs.showToast("Login failed", context);
                                FocusScope.of(context).requestFocus(new FocusNode());
                              }
                            }
                          },
                          text: "Sign In",
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

}