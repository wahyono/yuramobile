import 'package:flutter/material.dart';
import 'package:project_akhir/component/app_bar_custom.dart';
import 'package:project_akhir/component/login/login_button.dart';
import 'package:project_akhir/component/navigation/bottom_navigation_bar_custom.dart';
import 'package:project_akhir/service/shared_preferences_service.dart';

class Profile extends StatefulWidget {
  _Profile createState() => _Profile();
}

class _Profile extends State<Profile> {
  int _selectedIndex = 2;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.blue,
        flexibleSpace: SafeArea(
          child: AppBarCustom(),
        ),
      ),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(15),
          child: LoginButton(
            width: MediaQuery.of(context).size.width,
            height: 50,
            onTap: () async {
              await sharedPreferenceService.clearToken();
              Navigator.pushReplacementNamed(context, "/login");
            },
            text: "Logout",
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBarCustom(selectedIndex: _selectedIndex),
    );
  }

}