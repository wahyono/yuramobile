import 'package:flutter/material.dart';
import 'package:project_akhir/component/app_bar_custom.dart';
import 'package:project_akhir/component/dashboard/dashboard_profile.dart';
import 'package:project_akhir/component/navigation/bottom_navigation_bar_custom.dart';
import 'package:project_akhir/data/dashboard/dashboard_stats_data.dart';
import 'package:shared_preferences/shared_preferences.dart';

/**
 * ini adalah halaman dashboard yang jika token dari shared preferences tidak kosong, maka akan
 * di redirect ke halaman ini
 */

class Dashboard extends StatefulWidget {
  _Dashboard createState() => _Dashboard();
}

class _Dashboard extends State<Dashboard> {
  /**
   * variabel ini digunakan untuk menandai index bottom navigation bar
   */
  int _selectedIndex = 0;

  /**
   * variabel ini digunakan untuk menyimpan token dari shared preferences
   */
  String? token = "0";

  /**
   * variabel ini digunakan untuk menyimpan user id dari shared preferences
   */
  String? userId = "";

  /**
   * method di dalam init state akan di eksekusi saat pertama build
   */
  @override
  void initState() {
    super.initState();
    getStats();
  }

  /**
   * method asinkronous yang digunakan untuk mendapatkan data dari API
   */
  Future getStats() async {
    /**
     * mendefinisikan shared preferences
     */
    SharedPreferences prefs = await SharedPreferences.getInstance();

    /**
     * mendapatkan data dari API
     */
    var body = await DashboardStatsData().stats(prefs.getString("userId")!);

    /**
     * jika status code dari body == 200, maka body akan di return.
     * jika status code dari body != 200, false akan di return.
     */
    if(body != false) {
      return body;
    }

    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.blue,
        flexibleSpace: SafeArea(
          child: AppBarCustom(),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(15),
              /**
               * komponen dalam child ini akan menunggu hasil dari method getStats.
               * jika response API sukses, akan di create widget DahsboardProfile,
               * jika response API failed, akan di tampilkan progress indicator
               */
              child: FutureBuilder<dynamic> (
                future: getStats(),
                builder: (context, snapshot) {
                  if(snapshot.hasData) {
                    return DashboardProfile(data: snapshot.data);
                  } else {
                    return Center(child: CircularProgressIndicator());
                  }
                },
              ),
            )
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBarCustom(selectedIndex: _selectedIndex),
    );
  }

}