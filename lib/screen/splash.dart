import 'dart:async';

import 'package:flutter/material.dart';
import 'package:project_akhir/service/shared_preferences_service.dart';

/**
 * halaman ini yang akan pertama kali di run
 */

class Splash extends StatefulWidget {
  _Splash createState() => _Splash();
}

class _Splash extends State<Splash> {

  /**
   * method ini di binding pada fungsi build
   */
  initMethod(context) async {
    /**
     * mendapatkan data dari shared preferences
     */
    await sharedPreferenceService.getSharedPreferencesInstance();

    /**
     * mendapatkan token dari shared preferences
     */
    String? _token = await sharedPreferenceService.token;

    /**
     * kondisi jika _token dari shared preferences kosong
     */
    if(_token == null || _token == "") {
      /**
       * time untuk menunggu 3 detik sebelum redirect ke login
       */
      Timer(
          Duration(seconds: 3),
              () => Navigator.pushReplacementNamed(context, "/login")
      );
    } else {
      /**
       * jika _token tidak kosong, akan di redirect ke dashboard
       */
      Timer(
          Duration(seconds: 3),
              () => Navigator.pushReplacementNamed(context, "/dashboard")
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    /**
     * fungsi ini akan mem-binding method initMethod saat pertama kali build
     */
    WidgetsBinding.instance?.addPostFrameCallback((_) => initMethod(context));

    /**
     * komponen yang di tampilkan adalah sebuah gambar yang posisi nya terletak di tengah.
     * align center secara vertikal dan horizontal
     */
    return Scaffold(
        backgroundColor: Colors.white,
        body: Center(
          child: Image.asset("assets/images/icon.png"),
        )
    );
  }
  
}