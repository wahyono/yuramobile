import 'package:flutter/material.dart';

class OrdersList extends StatelessWidget {
  final data;
  const OrdersList({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String courier = "?";

    if(data["courier"] == "JNE") {
      courier = "jne";
    } else if(data["courier"] == "J&T") {
      courier = "jnt";
    } else if(data["courier"] == "NINJA") {
      courier = "ninja";
    } else if(data["courier"] == "SICEPAT") {
      courier = "sicepat";
    }

    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: Colors.grey, width: 0.5),
          borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          children: [
            Container(
              width: 50,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(8))
              ),
              child: Image.asset("assets/icons/" + courier + ".png")
            ),
            Container(
              child: Flexible(
                child: Container(
                  margin: EdgeInsets.only(left: 15, right: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 0, bottom: 2),
                        child: Text(
                          data["product"],
                          style: TextStyle(
                              fontWeight: FontWeight.w700,
                              fontSize: 16
                          ),
                        ),
                      ),
                      Flexible(
                        child: Container(
                          margin: EdgeInsets.only(bottom: 4),
                          child: Text(
                              data["c_name"]
                          ),
                        ),
                      ),
                      Flexible(
                        child: Container(
                          child: Text(
                              data["district"] + ", " + data["province"]
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}