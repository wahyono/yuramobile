import 'package:flutter/material.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:line_icons/line_icons.dart';

class BottomNavigationBarCustom extends StatelessWidget {
  const BottomNavigationBarCustom({Key? key, required this.selectedIndex}) : super(key: key);
  final int selectedIndex;
  @override
  Widget build(BuildContext context) {

    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(
              top: BorderSide(
                  color: Color(0xFFedebeb),
                  width: 1.0
              )
          )
      ),
      child: SafeArea(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 8),
          child: GNav(
            gap: 8,
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 12),
            tabBackgroundColor: Colors.blue,
            activeColor: Colors.white,
            tabs: [
              GButton(
                icon: LineIcons.home,
                text: "Dashboard",
              ),
              GButton(
                icon: LineIcons.book,
                text: "Orders",
              ),
              GButton(
                icon: LineIcons.user,
                text: "Profile",
              )
            ],
            selectedIndex: selectedIndex,
            onTabChange: (index) {
              if(index == 0) {
                Navigator.pushReplacementNamed(context, "/dashboard");
              } else if(index == 1) {
                Navigator.pushReplacementNamed(context, "/orders");
              } else if(index == 2) {
                Navigator.pushReplacementNamed(context, "/profile");
              }
            },
          ),
        ),
      ),
    );

  }



}