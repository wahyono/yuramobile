import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {

  late final VoidCallback onTap;
  late final String text;
  late final double height;
  late final double width;

  LoginButton({
    required this.onTap,
    required this.text,
    required this.height,
    required this.width
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFFFF971D),
                  Color(0xFFFD8833)
                ]
            )
        ),
        child: Center(
          child: Text(
            text,
            style: TextStyle(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w600
            ),
          ),
        ),
      ),
    );
  }

}