import 'package:flutter/material.dart';
import 'package:project_akhir/component/dashboard/dashboard_stats.dart';

class DashboardProfile extends StatelessWidget {
  final data;
  const DashboardProfile({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      runSpacing: 10,
      children: [
        Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                  border: Border.all(color: Colors.grey, width: 0.5),
                  borderRadius: BorderRadius.all(Radius.circular(8))
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 100,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(8),
                            topRight: Radius.circular(8)
                        )
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(15),
                      child: Column(
                        children: [
                          Text(
                            data["user"]["name"],
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w700,
                                fontSize: 25
                            ),
                          ),
                          Text(
                            data["user"]["position"],
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 16
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    height: 150,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(8),
                            bottomRight: Radius.circular(8)
                        )
                    ),
                    child: Container(
                      margin: EdgeInsets.only(top: 60),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Text(
                                  data["totalSales"]["count"].toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "SALES",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            width: 1,
                            height: 50,
                            child: Container(
                                color: Colors.grey.shade200
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Text(
                                  data["todaySales"]["count"].toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "TODAY SALES",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            width: 1,
                            height: 50,
                            child: Container(
                                color: Colors.grey.shade200
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Container(
                                child: Text(
                                  data["user"]["product_type"],
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 16
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  "PRODUCT",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 250,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    alignment: Alignment.center,
                    height: 90,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.grey, width: 0.5),
                        shape: BoxShape.circle
                    ),
                    child: Image.asset("assets/images/icon.png"),
                  )
                ],
              ),
            )
          ],
        ),
        Wrap(
          runSpacing: 10,
          children: [
            DashboardStats(
              title: "Most used delivery courier",
              value: data["mostUsedCourier"]["courier"] + " - " + data["mostUsedCourier"]["count"].toString(),
            ),
            DashboardStats(
              title: "Most used warehouse",
              value: data["mostWarehouseCourier"]["warehouse"] + " - " + data["mostWarehouseCourier"]["count"].toString(),
            ),
            DashboardStats(
              title: "Most used payment method",
              value: data["mostUsedPaymentMethod"]["payment_method"] + " - " + data["mostUsedPaymentMethod"]["count"].toString(),
            ),
            DashboardStats(
              title: "Most sold product",
              value: data["mostSoldProduct"]["product_name"] + " - " + data["mostSoldProduct"]["count"].toString(),
            ),
            DashboardStats(
              title: "Most sent destination",
              value: data["mostProvince"]["province"] + " - " + data["mostProvince"]["count"].toString(),
            ),
            DashboardStats(
              title: "Total price",
              value: data["totalPrice"]["price"],
            )
          ],
        )
      ],
    );
  }

}