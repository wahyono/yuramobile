import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DashboardStats extends StatelessWidget {
  final title;
  final value;
  const DashboardStats({Key? key, required this.title, required this.value}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey, width: 0.5),
        borderRadius: BorderRadius.all(Radius.circular(8))
      ),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          children: [
            Container(
              height: 60,
              width: 50,
              decoration: BoxDecoration(
                  color: Colors.pink,
                  borderRadius: BorderRadius.all(Radius.circular(8))
              ),
              child: Icon(Icons.person_outline_sharp),
            ),
            Container(
              child: Flexible(
                child: Container(
                  height: 60,
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10, bottom: 5),
                        child: Text(
                            title
                        ),
                      ),
                      Container(
                        child: Text(
                          value,
                          style: TextStyle(
                              fontWeight: FontWeight.w700
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

}