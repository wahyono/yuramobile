import 'dart:convert';
import 'package:http/http.dart';

class DashboardStatsData {
  Future stats(String userId) async {
    print("stats : " + userId);
    final jsonBody = jsonEncode({"user": userId});
    final Map<String,String> jsonHeader = {"Content-Type": "application/json"};

    var response = await post(Uri.parse("https://yura.my.id/api/stats"), body: jsonBody, headers: jsonHeader);

    if(response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return false;
    }
  }
}