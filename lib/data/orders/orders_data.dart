import 'dart:convert';
import 'package:http/http.dart';

class OrdersData {
  Future stats(String userId) async {
    final jsonBody = jsonEncode({"user": userId, "page": 1, "paginate": 10});
    final Map<String,String> jsonHeader = {"Content-Type": "application/json"};

    var response = await post(Uri.parse("https://yura.my.id/api/orders"), body: jsonBody, headers: jsonHeader);

    if(response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return false;
    }
  }
}