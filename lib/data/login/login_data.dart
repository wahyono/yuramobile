import 'dart:convert';
import 'package:http/http.dart';

class LoginData {
  Future login(String email, String password) async {
    final jsonBody = jsonEncode({"email": email, "password": password});
    final Map<String,String> jsonHeader = {"Content-Type": "application/json"};

    var response = await post(Uri.parse("https://yura.my.id/api/login"), body: jsonBody, headers: jsonHeader);

    if(response.statusCode == 200) {
      return jsonDecode(response.body);
    } else {
      return false;
    }
  }
}