import 'package:flutter/material.dart';
import 'package:project_akhir/screen/dashboard.dart';
import 'package:project_akhir/screen/login.dart';
import 'package:project_akhir/screen/orders.dart';
import 'package:project_akhir/screen/profile.dart';
import 'package:project_akhir/screen/splash.dart';

/**
 *
 * halaman main adalah halaman utama yang bertindak sebagai MainActivity. artinya halaman ini yang akan
 * pertama kali di run.
 *
 */

/**
 * fungsi ini yang akan menjalankan aplikasi
 */
void main() => runApp(MyApp());


/**
 * MyApp adalah class utama dalam file ini yang memiliki child _MyAppState
 */
class MyApp extends StatefulWidget {
  /**
   * semua komponen akan dibuat menggunakan fungsi ini
   */
  _MyAppState createState() => _MyAppState();
}

/**
 * class untuk build komponen
 */
class _MyAppState extends State<MyApp> {

  @override
  Widget build(BuildContext context) {
    /**
     * mendefinisikan route dalam aplikasi ini
     */
    final routes = <String, WidgetBuilder> {
      "/dashboard": (BuildContext context) => Dashboard(),
      "/orders": (BuildContext context) => Orders(),
      "/login": (BuildContext context) => Login(),
      "/profile": (BuildContext context) => Profile()
    };

    /**
     * komponen yang di "kembalikan" dalam bentuk MaterialApp. dalam komponen ini halaman yang akan
     * pertama kali di run adalah halaman Splash.
     */
    return MaterialApp(
      title: "YuRa MobileApp",
      theme: ThemeData(
          primaryColor: Colors.black
      ),
      routes: routes,
      home: Splash(),
    );
  }

}